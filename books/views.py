from CoBo.books.forms import CreateBookForm, CreateChapterForm
from CoBo.books.models import Book, Chapter
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.utils.translation import ugettext as _
from guardian.shortcuts import assign
import datetime

def overview(request):
    page = request.GET.get('page')
    book_list = Book.objects.all()
    paginator = Paginator(book_list, 25)
    
    if page is None:
        page = 1
    try:
        books = paginator.page(page)
    except PageNotAnInteger:
        books = paginator.page(1)
    except EmptyPage:
        books = paginator.page(paginator.num_pages)
    return render_to_response('books/overview.html', {"books": books}, context_instance=RequestContext(request))
        
        
def show(request, book_id):
    output = {}
    book = Book.objects.get(id=book_id)
    output['book'] = book
    mode_time = book.last_switch + datetime.timedelta(0,book.period_length)
    if mode_time<datetime.datetime.now():
        book.last_switch = datetime.datetime.now()
        mode_time = book.last_switch
        if book.writing_enabled and proposalsAdded(book):
            book.writing_enabled = False
        elif not book.writing_enabled:
            book.writing_enabled = True
            proposals = Chapter.objects.all().filter(book__exact=book, accepted__exact=False, num__exact=getLastAccepted(book).num + 1)
            winner = sorted(proposals, key=lambda proposal: proposal.votes.count())[0]
            winner.accepted = True
            winner.save()        
        book.save()
    output['mode_time'] = mode_time
    chapters = Chapter.objects.filter(accepted__exact=True, book__exact=book)
    output['chapters'] = chapters
    proposals = Chapter.objects.filter(accepted__exact=False, book__exact=book, num__exact = len(chapters))
    output['proposals'] = proposals
    output['manage'] = book.isManager(request.user)
    return render_to_response('books/show.html', output, context_instance=RequestContext(request))

@login_required()
def new(request):
    if request.method == 'POST':
        form = CreateBookForm(request.POST)
        if form.is_valid():
            book = Book()
            book.creator = request.user;
            book.title = form.cleaned_data['title']
            book.tags = form.cleaned_data['tags'].replace(' ', '')
            book.text =  form.cleaned_data['tags']
            book.restricted_read = form.cleaned_data['read_permission']
            book.restricted_write = form.cleaned_data['write_permission']
            book.save()
            chapter=Chapter()
            chapter.book = book;
            chapter.accepted = True;
            chapter.num = 0;
            chapter.creator = request.user;
            chapter.text = form.cleaned_data['text']
            chapter.save()
            assign('write_book', request.user, book)
            assign('read_book', request.user, book)
            assign('manage_book', request.user, book)
            return redirect(reverse("CoBo.books.views.show",args=[book.id]))
    else:
        form = CreateBookForm()
    return render_to_response('books/new.html', {"form": form}, context_instance=RequestContext(request))

@login_required()
def new_chapter(request, book_id):
    book = Book.objects.get(id=book_id)
    if not book.restricted_write or request.user.has_perm('books.write_book', book_id):
        last_chapter = getLastAccepted(book)
        if request.method == 'POST':
            form = CreateChapterForm(request.POST)
            if form.is_valid():
                chapter = Chapter()
                chapter.book = book
                chapter.creator = request.user
                chapter.text = form.cleaned_data['text']
                chapter.num = last_chapter.num + 1
                chapter.accepted = False
                chapter.save()
                return redirect(reverse("CoBo.books.views.show",args=[book_id]))
        else:
            form = CreateChapterForm()
    return render_to_response('books/new_chapter.html',{"form": form, "book": book, "chapter": last_chapter}, context_instance=RequestContext(request))

@login_required()
def manage_book(request, id):
    book = Book.objects.get(id=id)
    if not book.isManager(request.user):
        return HttpResponseForbidden();
    return 0

@login_required()
def manage_book_switch(request, id):
    book = Book.objects.get(id=id)
    if not book.isManager(request.user):
        return HttpResponseForbidden();
    book.writing_enabled = (book.writing_enabled == False)
    book.last_switch = datetime.datetime.now()
    book.save()
    return redirect(reverse("CoBo.books.views.show",args=[book.id]))

@login_required()
def manage_book_delete(request, id):
    book = Book.objects.get(id=id)
    if not book.isManager(request.user):
        return HttpResponseForbidden();
    return 0

@login_required()
def manage_chapter(request, mode, id):
    #FIXME: Add check if authorized 
    if mode=="delete":
        chapter = Chapter.objects.get(id=id)
        if request.method == 'POST':
            chapter.delete()
            return redirect(reverse("CoBo.books.views.show",args=[chapter.book.id]))
        return render_to_response('books/approve.html', {"id": id, "mode": mode, "action": _("delete"), "chapter": chapter}, context_instance=RequestContext(request))
    if mode=="promote":
        chapter = Chapter.objects.get(id=id)
        if request.method == 'POST':
            chapter.accepted = True
            chapter.save()
            book = chapter.book
            book.writing_enabled = True
            book.last_switch = datetime.datetime.now()
            book.save()
            return redirect(reverse("CoBo.books.views.show",args=[chapter.book.id]))
        return render_to_response('books/approve.html', {"id": id, "mode": mode, "action": _("promote"), "chapter": chapter}, context_instance=RequestContext(request))
    raise Http404

@login_required()
def manage_chapter_edit(request, id):
    #FIXME: Add check if authorized 
    chapter = Chapter.objects.get(id=id)

    return 0

@login_required()
def vote(request, id):
    #FIXME: Add check if authorized 
    chapter = Chapter.objects.get(id=id)
    if(request.user in chapter.votes.all()):
        chapter.votes.remove(request.user)
    else:
        chapter.votes.add(request.user)
    return redirect(reverse("CoBo.books.views.show",args=[chapter.book.id]))


def getLastAccepted(book):
    return book.chapter_set.filter(accepted=True).latest("num")
    
def proposalsAdded(book):
    num = getLastAccepted(book).num + 1
    return book.chapter_set.filter(accepted=False, num=num).count() > 0