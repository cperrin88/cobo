from django import forms
from django.utils.translation import ugettext as _

class CreateBookForm(forms.Form):
    title = forms.CharField(max_length=100, label = _("Title"), help_text=_("Enter a title for your book. Remeber: It will be the first thing that the readers will see!"))
    text = forms.CharField(widget=forms.Textarea, label = _("Text"))
    tags = forms.CharField(label = _("Tags"))
    read_permission = forms.BooleanField(label=_("Restricted reading"), required=False)
    write_permission = forms.BooleanField(label=_("Restricted writing"), required=False)
    
class CreateChapterForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea, label = _("Text"))
    