from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length="200")
    creator = models.ForeignKey(User)
    tags = models.CharField(max_length="500")
    creation = models.DateTimeField(auto_now_add=True)
    writing_enabled = models.BooleanField(default=True)
    last_switch = models.DateTimeField(auto_now_add=True)
    period_length = models.IntegerField(default=604800)
    restricted_read = models.BooleanField(default=False)
    restricted_write = models.BooleanField(default=False)
    def __unicode__(self):
        return "%s (ID: %d)" % (self.title, self.id)
    
    def getName(self):
        if len(self.creator.first_name) != 0 or len(self.creator.last_name):
            return "%s %s" % (self.creator.first_name, self.creator.last_name)
        else:
            return self.creator.username
    
    def isManager(self, user):
        return user == self.creator
    
    class Meta:
        permissions = (('read_book', "Read a book",),
                      ('write_book', "Write new chapters",),
                      ('manage_book', "Manage a book",),)
    
     
class Chapter(models.Model):
    creator = models.ForeignKey(User)
    book = models.ForeignKey(Book)
    text = models.TextField()
    num = models.IntegerField()
    accepted = models.BooleanField()
    creation = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)
    votes = models.ManyToManyField(User, related_name="vote")
    def __unicode__(self):
        return "%s: Chapter %d - %s (id %d)" % (self.book.title, self.num, self.creator, self.id)
    
    def getName(self):
        if len(self.creator.first_name) != 0 or len(self.creator.last_name):
            return "%s %s" % (self.creator.first_name, self.creator.last_name)
        else:
            return self.creator.username
        