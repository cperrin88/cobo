"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from CoBo.books.models import Book, Chapter
from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import Client
import datetime

class BookTest(TestCase):
    def setUp(self):
        user = User.objects.create_user("Test", "test@test.com", "Test")
        user.is_staff = True
        user.save()
        self.user = user;
        book = Book()
        book.title = ("Lorem Ipsum")
        book.creator = user
        book.tags = ("Test")
        book.writing_enabled = True
        book.save()
        book.last_switch = datetime.datetime(datetime.MINYEAR,1,1)
        book.save()
        chapter = Chapter()
        chapter.creator = user
        chapter.book = book
        chapter.text = "Lorem Ipsum Dolor Sit Amet"
        chapter.num = 0
        chapter.accepted = True
        chapter.save()
        
    def tearDown(self):
        Book.objects.all().delete()
        Chapter.objects.all().delete()
                
    def test_modeswitch_no_proposals(self):
        c = Client()
        book = Book.objects.all()[0]
        c.get("/books/show/%d/" % book.id);
        book = Book.objects.all()[0] 
        self.assertEqual(book.writing_enabled, True)
        self.assertNotEqual(book.last_switch, datetime.datetime(datetime.MINYEAR,1,1), "Last Switch: %s" % book.last_switch)
        
    def test_modeswitch(self):
        c = Client()
        book = Book.objects.all()[0]
        book.writing_enabled = False
        book.save()
        chapter = Chapter()
        chapter.creator = self.user
        chapter.book = book
        chapter.text = "Lorem Ipsum Dolor Sit Amet"
        chapter.num = 1
        chapter.accepted = False
        chapter.save()
        chapter.votes.add(self.user);
        self.assertEqual(c.get("/books/show/%d/" % book.id).status_code, 200)
        book = Book.objects.all()[0] 
        self.assertEqual(book.writing_enabled, True)
        self.assertNotEqual(book.last_switch, datetime.datetime(datetime.MINYEAR,1,1), "Last Switch: %s" % book.last_switch)
        self.assertNotEqual(Chapter.objects.filter(book__exact=book, accepted__exact=True, num__exact=1).count(), 0)
