from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'CoBo.mainpage.views.mainpage'),
    url(r'^books/', include("CoBo.books.urls")),
    url(r'^accounts/', include("CoBo.accounts.urls")),
    url(r'^admin/', include(admin.site.urls)),
)
