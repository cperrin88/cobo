from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext

def mainpage(request):
    
    return render_to_response('mainpage/base.html', {}, context_instance=RequestContext(request))