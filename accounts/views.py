from CoBo.accounts.models import MessageBox, Messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext

@login_required()
def profile(request, user_id=None):
    if user_id is None:
        user = request.user
    else:
        user = User.objects.get(id=user_id)
    return render_to_response("accounts/profile.html", {"profile": user}, context_instance=RequestContext(request))

def register(request):
    #TODO: Redirect to last page
    if request.method == "POST":
        uform = UserCreationForm(request.POST)
        if uform.is_valid():
            uform.save()
            return redirect("/")
    uform = UserCreationForm()
    return render_to_response("accounts/register.html", {"form": uform}, context_instance=RequestContext(request))

@login_required()
def messagebox(request, mbox = None):
    if MessageBox.objects.filter(owner=request.user).count() == 0:
            MessageBox.create_intial_mboxes(request.user)
    mboxes = MessageBox.objects.filter(owner=request.user)
    if mbox is None:
        messages = mboxes.filter(system_name = "inbox").get().messages_set.all()
    else:
        messages = mboxes.get(id = mbox).messages_set.all()
    return render_to_response("accounts/messages.html", {"messageboxes": mboxes, "messages": messages}, context_instance=RequestContext(request))

def new_message(request):
    if request.method == "POST":
        pass