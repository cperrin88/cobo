from django.conf.urls.defaults import patterns
from django.contrib.auth.forms import AuthenticationForm

urlpatterns = patterns('',
    (r'^login/$', 'django.contrib.auth.views.login', {'authentication_form': AuthenticationForm, 'template_name': 'accounts/login.html',}),
    (r'^logout/', 'django.contrib.auth.views.logout', {'next_page': '/', }),
    (r'^register/', 'CoBo.accounts.views.register'),
    (r'^profile/$', 'CoBo.accounts.views.profile'),
    (r'^profile/(?P<user_id>\d+)/$', 'CoBo.accounts.views.profile'),
    (r'^messages/$', 'CoBo.accounts.views.messagebox'),
    (r'^messages/(?P<mbox>\d+)/$', 'CoBo.accounts.views.messagebox'),
)
