from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_noop as _

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    

class MessageBox(models.Model):    
    owner = models.ForeignKey(User)
    name = models.CharField(max_length="200")
    priority = models.IntegerField(default=0)
    system = models.BooleanField()
    system_name = models.CharField(max_length="200")
    class Meta:
        ordering = ['-priority', 'name']
    
    @staticmethod
    def create_intial_mboxes(user):
        MessageBox(owner=user, name=_("Inbox"), priority="10", system = True, system_name = "inbox").save()
        MessageBox(owner=user, name=_("Sent"), priority="9", system = True, system_name = "sent").save()
        MessageBox(owner=user, name=_("Thrash"), priority="8", system = True, system_name = "thrash").save()
        
    
class Messages(models.Model):
    sender = models.ForeignKey(User, related_name = "messages_sent_set")
    receiver = models.ForeignKey(User, related_name = "messages_received_set")
    MessageBox = models.ForeignKey(MessageBox)
    title = models.CharField(max_length="200")
    text = models.TextField()
    read = models.BooleanField(default=False)
    datetime = models.DateField(auto_now_add=True)